dir=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/ntuples/FullRun2_Boosted/mc16d_dijet
user=user.cdelitzs.
outdir=/eos/user/n/nlopezca/hbbisr_projv2/outputs/mc16d_bg_v9_m0cut_unw
configdir=/eos/user/n/nlopezca/hbbisr_projv2/ZprimeDM/data

for file in ${dir}/${user}3647*.Pythia8* 
do
    STR=$file
    SUBSTR=$(echo $file | cut -d'.' -f 3)
    echo $SUBSTR

    ls $file/* > tmplist.list

    xAH_run.py --files tmplist.list --inputList -f --submitDir $outdir/OUT_Pythia8_$SUBSTR --config $configdir/config_jetJet.py --treeName outTree --isMC  direct

    rm tmplist.list
    #P= $(( ${#dir} + ${#user} ))
    #echo $P
    #echo $file | cut -c$P-(( $P + 6 ))
    echo " -------------"
done

#reweigth histograms by DISD code                                                                      
#echo "Starting reweighting" 
#for file in $outdir/OUT_Pythia8*

#do
 #   applyMiniTreeEventCountWeight.py $file
#done

#merge histograms          
#echo "Merge output"  
#hadd $outdir/hist-Pythia8_dijet.dijet.NTUP.root $outdir/OUT_Pythia8_*/hist-*.root

